# Depoxy

Depoxy's goal is to provide a centralised server that acts as a proxy for
accessing files on different machines.

The first use case in mind is having the proxy service `depoxy-server` running
in a DMZ, to which a client `depoxy-feeder` running on a NAS connects to. A
remote user connects on the web interface of `depoxy-server` and selects a file
that will be retrieved from `depoxy-feeder`.

## Terminology

- `anchor` directory: a root for source directories situated at a given `depth`
- `source` directory: a directory which contains source material

For example:

    mnt
    ├─ music                    (anchor directory, depth 2)
    │  └─ a
    │     └─ AC⚡DC             (source directory)
    │        └─ Back In Black
    └─ videos                   (anchor directory, depth 1)
       ├─ movies                (source directory)
       │  └─ Terminator 2
       └─ series                (source directory)
          └─ Breaking Bad

will appear to a user as a list like this:

    - Back In Black
    - Terminator 2
    - Breaking Bad
