module Feeder (
    main,
) where

import Control.Monad
import qualified Data.ByteString.Lazy as BL
import Data.Maybe
import qualified Data.Text.Lazy as TL
import Data.UUID (toByteString)
import Data.UUID.V4 (nextRandom)
import System.Exit
import System.ZMQ4.Monadic

import qualified Feeder.Args as A
import qualified Feeder.Cataloguer as Cat
import qualified Feeder.Config as C
import qualified Feeder.Database as D
import qualified Feeder.Database.AnchorDir as DA
import qualified Feeder.Database.Global as DG
import qualified Feeder.Worker as W

connectInitDb :: C.Config -> IO D.DBCon
connectInitDb config = do
    -- open the database connection
    dbCon <- D.getConnection $ C.dbPath $ C.database config

    D.init dbCon
    anchors <- D.getAnchorDirs dbCon

    -- if and only if the list is empty, add the anchor directories from the
    -- config file into the database
    when (null anchors) $
        forM_ (C.anchors config) $
            \(C.AnchorDir f d) ->
                let anchorDir = DA.mkAnchorDir (TL.pack f) d
                 in D.addAnchorDir dbCon anchorDir

    return dbCon

main :: IO ()
main = do
    -- read command-line arguments
    args <- A.parseArgs

    -- print version and quit
    when
        (A.version args)
        ( do
            print "0.0.1"
            exitSuccess
        )

    -- read config file
    cf <- BL.readFile $ A.config args
    let config = fromJust $ C.decode cf

    -- get a connection object to the database and initialise it
    dbCon <- connectInitDb config
    dbUuid <- DG.uuid <$> D.getGlobal dbCon

    -- create a new socket ID for receiving the server's requests
    socketId <- nextRandom

    runZMQ $ do
        -- Cataloguer thread
        _ <- async $ Cat.cataloguerLoop config dbCon socketId

        -- Socket for listening to file download requests
        sockServer <- socket Dealer
        setIdentity (restrict $ BL.toStrict $ toByteString socketId) sockServer
        connect sockServer $ C.request $ C.server_addresses config

        -- Socket for fanning-out file download requests
        sockWorkers <- socket Dealer
        bind sockWorkers W.addrWorkers

        -- Worker threads
        replicateM_ (C.workers config) (async $ W.worker dbCon dbUuid Cat.addrCataloguer)

        -- Use ZMQ's proxy loop for messages between server and workers
        proxy sockServer sockWorkers Nothing
