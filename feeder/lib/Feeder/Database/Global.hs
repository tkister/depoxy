{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}

module Feeder.Database.Global (
    Global (..),
    createGlobal,
    init,
    getGlobal,
    updateRevision,
) where

import Control.Monad
import Data.Int
import Data.Maybe
import Data.UUID
import Data.UUID.V4
import Database.SQLite.Simple
import Database.SQLite.Simple.FromField
import Database.SQLite.Simple.Internal
import Database.SQLite.Simple.Ok
import GHC.Generics
import Text.RawString.QQ
import Prelude hiding (init)

data Global = Global
    { uuid :: UUID
    , revision :: Int64
    }
    deriving (Generic, Show)

newtype WUUID = WUUID {runWuuid :: UUID}

mkGlobal :: WUUID -> Int64 -> Global
mkGlobal wuuid = Global (runWuuid wuuid)

instance FromField WUUID where
    fromField f@(Field (SQLText wuuid) _) =
        let value = fromText wuuid
         in maybe (returnError ConversionFailed f "for UUID") Ok (WUUID <$> value)
    fromField f = returnError ConversionFailed f "for UUID"

instance FromRow Global where
    fromRow = mkGlobal <$> field <*> field

createGlobal :: Query
createGlobal =
    [r|
        CREATE TABLE IF NOT EXISTS global(
            uuid TEXT NOT NULL,
            revision INTEGER(8) NOT NULL
        )
    |]

init :: Connection -> IO ()
init conn = do
    [[count]] <- query_ conn [r| SELECT COUNT(*) FROM global |] :: IO [[Int]]

    when (count == 0) $ do
        new_uuid <- nextRandom
        executeNamed
            conn
            [r| 
                INSERT INTO global(uuid, revision)
                VALUES(:uuid, 1)
            |]
            [":uuid" := toString new_uuid]

getGlobal :: Connection -> IO Global
getGlobal conn = do
    [global] <-
        query_ conn [r| SELECT uuid, revision FROM global |]

    return global

updateRevision :: Connection -> IO ()
updateRevision conn = do
    [[old_revision]] <-
        query_ conn [r| SELECT revision FROM global |] :: IO [[Int64]]

    executeNamed
        conn
        [r| UPDATE global SET revision = :revision|]
        [":revision" := old_revision + 1]
