{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE MultiWayIf #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TemplateHaskell #-}

module Feeder.Database.File (
    File (..),
    mkFile,
    createFile,
    getFile,
    getFiles,
    addFile,
    onlyKeepFiles,
) where

import Control.Monad
import Data.Int
import qualified Data.Set as Set
import qualified Data.Text.Lazy as TL
import Data.Time
import Database.SQLite.Simple
import GHC.Generics
import Text.RawString.QQ
import Prelude hiding (id)

import qualified Feeder.Database.Global as G

data File = File
    { id :: Int64
    , absolutePath :: TL.Text
    , relativePath :: TL.Text
    , size :: Int64
    , timestamp :: UTCTime
    , anchorDirId :: Int64
    }
    deriving (Generic, Show)

mkFile :: TL.Text -> TL.Text -> Int64 -> UTCTime -> Int64 -> File
mkFile = File (-1)

instance FromRow File where
    fromRow = File <$> field <*> field <*> field <*> field <*> field <*> field

instance ToRow File where
    toRow file =
        toRow $
            (,,,,) <$> absolutePath
                <*> relativePath
                <*> size
                <*> timestamp
                <*> anchorDirId
                $ file

createFile :: Query
createFile =
    [r|
        CREATE TABLE IF NOT EXISTS file(
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            absolute_path TEXT NOT NULL UNIQUE,
            relative_path TEXT NOT NULL,
            size INTEGER(8) NOT NULL,
            timestamp TEXT NOT NULL,
            anchor_dir_id INTEGER NOT NULL REFERENCES anchor_dir(id)
        )
    |]

getFile :: Connection -> Int64 -> IO (Maybe File)
getFile conn fileId = do
    rs <-
        queryNamed
            conn
            [r| SELECT * FROM file WHERE id = :fileId |]
            [":fileId" := fileId]
    let file = if null rs then Nothing else Just $ head rs
    pure file

getFiles :: Connection -> IO [File]
getFiles conn = do
    query_ conn [r| SELECT * FROM file |]

_addFileInsert :: ToRow q => Connection -> q -> IO ()
_addFileInsert conn file =
    execute
        conn
        [r|
            INSERT INTO file(
                absolute_path
                , relative_path
                , size
                , timestamp
                , anchor_dir_id
            )
            VALUES(?, ?, ?, ?, ?)
        |]
        file

_addFileUpdate :: Connection -> File -> IO ()
_addFileUpdate conn file =
    executeNamed
        conn
        [r|
            UPDATE file
            SET size = :size
                , timestamp = :timestamp
            WHERE absolute_path = :absolute_path
        |]
        [ ":size" := size file
        , ":timestamp" := timestamp file
        , ":absolute_path" := absolutePath file
        ]

_addFileCaseOf :: Connection -> File -> [File] -> IO ()
_addFileCaseOf conn file rs
    | null rs = do
        _addFileInsert conn file
        G.updateRevision conn
    | timestamp (head rs) < timestamp file = do
        _addFileUpdate conn file
        G.updateRevision conn
    | otherwise = pure ()

addFile :: Connection -> File -> IO File
addFile conn file = do
    rs <-
        queryNamed
            conn
            [r|
                SELECT * FROM file
                WHERE absolute_path = :absolute_path
            |]
            [":absolute_path" := absolutePath file]

    _addFileCaseOf conn file rs

    [[fileId]] <-
        queryNamed
            conn
            [r|
                SELECT id
                FROM file
                WHERE absolute_path = :absolute_path
            |]
            [":absolute_path" := absolutePath file] ::
            IO [[Int64]]

    return $ file{id = fileId}

onlyKeepFiles :: Connection -> [File] -> IO ()
onlyKeepFiles _ [] = return ()
onlyKeepFiles conn fs =
    withTransaction conn $ do
        ids <- query_ conn [r| SELECT id FROM file ORDER BY 1 |] :: IO [[Int64]]

        let old_ids = Set.fromAscList $ head <$> ids
        let new_ids = Set.fromList $ id <$> fs
        let rem_ids = Set.difference old_ids new_ids

        mapM_
            ( \x ->
                executeNamed
                    conn
                    [r| DELETE FROM file WHERE id = :id |]
                    [":id" := x]
            )
            rem_ids
        G.updateRevision conn
