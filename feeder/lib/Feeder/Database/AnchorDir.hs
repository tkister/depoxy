{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TemplateHaskell #-}

module Feeder.Database.AnchorDir (
    AnchorDir (..),
    mkAnchorDir,
    createAnchorDir,
    getAnchorDirs,
    addAnchorDir,
) where

import Data.Int
import qualified Data.Text.Lazy as TL
import Database.SQLite.Simple
import Text.RawString.QQ
import Prelude hiding (id)

data AnchorDir = AnchorDir
    { id :: Int64
    , path :: TL.Text
    , depth :: Int
    }
    deriving (Show)

mkAnchorDir :: TL.Text -> Int -> AnchorDir
mkAnchorDir = AnchorDir (-1)

instance FromRow AnchorDir where
    fromRow = AnchorDir <$> field <*> field <*> field

instance ToRow AnchorDir where
    toRow anchorDir = toRow $ (,) <$> path <*> depth $ anchorDir

createAnchorDir :: Query
createAnchorDir =
    [r|
        CREATE TABLE IF NOT EXISTS anchor_dir(
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            path TEXT NOT NULL UNIQUE,
            depth INTEGER NOT NULL
        )
    |]

getAnchorDirs :: Connection -> IO [AnchorDir]
getAnchorDirs conn = do
    query_
        conn
        [r| 
            SELECT id, path, depth
            FROM anchor_dir 
        |]

addAnchorDir :: Connection -> AnchorDir -> IO AnchorDir
addAnchorDir conn anchorDir = do
    executeNamed
        conn
        [r|
            INSERT INTO anchor_dir(path, depth)
            VALUES(:path, :depth)
            ON CONFLICT(path) DO UPDATE SET depth = excluded.depth
        |]
        [":path" := path anchorDir, ":depth" := depth anchorDir]

    [[anchorDirId]] <-
        queryNamed
            conn
            [r|
                SELECT id
                FROM anchor_dir
                WHERE path = :path
            |]
            [":path" := path anchorDir] ::
            IO [[Int64]]

    return anchorDir{id = anchorDirId}
