module Feeder.Cataloguer.FsListener (
    ListenerEvent,
    start,
) where

import Control.Concurrent
import Control.Concurrent.STM
import Control.Monad
import System.FSNotify as FS
import Prelude hiding (pred)

data ListenerEvent = Ready | Change deriving (Eq, Show)

untilM :: (a -> Bool) -> IO (a) -> IO ()
untilM pred action = do
    res <- action
    if pred res then return () else untilM pred action

-- Starts an FS Notify Manager which sends FS events on a TQueue.
-- Meant to be run on a dedicated thread as it sleeps forever.
listenNotification :: [FilePath] -> TQueue ListenerEvent -> IO ()
listenNotification ds queue = do
    withManager $ \mgr -> do
        let accept = const True
            enqueue = const $ atomically $ writeTQueue queue Change
            watch d = watchDir mgr d accept enqueue
         in mapM_ watch ds
        -- TODO the return values should not be discarded?

        tid <- myThreadId
        putStrLn $ "Thread " ++ show tid ++ " monitoring file system"

        -- signal that the manager is now listening to all directories
        atomically $ writeTQueue queue Ready

        -- sleep forever
        forever $ threadDelay (maxBound :: Int)

start :: [FilePath] -> IO (TQueue ListenerEvent)
start ds = do
    -- TQueue for receiving FS events
    queue <- newTQueueIO :: IO (TQueue ListenerEvent)

    -- Start monitoring the anchor directories and block until it has been setup
    _ <- forkIO $ listenNotification ds queue
    untilM ((==) Ready) (atomically $ readTQueue queue)

    pure queue
