{-# LANGUAGE TupleSections #-}

module Feeder.Cataloguer.RefreshFiles (
    refreshFiles,
) where

import Control.Exception
import Control.Monad
import Data.Int
import qualified Data.Text.Lazy as TL
import System.Directory
import System.FilePath
import Prelude hiding (abs, id)

import qualified Feeder.Database as D
import qualified Feeder.Database.AnchorDir as DA
import qualified Feeder.Database.File as DF

type AnchorPath = FilePath

getDirContents' :: AnchorPath -> Int64 -> FilePath -> IO [DF.File]
getDirContents' anchorPath anchorDirId rel = do
    let abs = anchorPath </> rel
    isFileAndExists <- doesFileExist abs

    if isFileAndExists
        then do
            sz <- getFileSize abs
            ts <- getModificationTime abs
            return
                [ DF.mkFile
                    (TL.pack abs)
                    (TL.pack rel)
                    (fromInteger sz)
                    ts
                    anchorDirId
                ]
        else do
            ds <- (fmap . fmap) (rel </>) (listDirectory abs)
            join <$> mapM (getDirContents' anchorPath anchorDirId) ds

ignoreException :: SomeException -> IO [DF.File]
ignoreException = const $ return []

getDirContents :: (AnchorPath, Int64) -> IO [DF.File]
getDirContents (anchorPath, anchorDirId) = do
    abs <- makeAbsolute anchorPath
    t <- try $ getDirContents' abs anchorDirId ""
    either ignoreException return t

getSourceDirs' :: FilePath -> Int -> IO [FilePath]
getSourceDirs' f d
    | d == 0 = return [f]
    | otherwise = do
        entries <- (fmap . fmap) (f </>) (listDirectory f)
        children <- filterM doesDirectoryExist entries
        join <$> mapM (`getSourceDirs'` (d - 1)) children

getSourceDirs :: DA.AnchorDir -> IO [(FilePath, Int64)]
getSourceDirs (DA.AnchorDir id path depth) = do
    dirs <- getSourceDirs' (TL.unpack path) depth
    return $ (,id) <$> dirs

refreshFiles :: [DA.AnchorDir] -> D.DBCon -> IO ()
refreshFiles paths dbCon = do
    folders <- join <$> mapM getSourceDirs paths
    files <- join <$> mapM getDirContents folders
    addedFiles <- mapM (D.addFile dbCon) files
    D.onlyKeepFiles dbCon addedFiles
