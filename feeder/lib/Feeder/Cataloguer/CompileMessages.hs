module Feeder.Cataloguer.CompileMessages (
    compileFeederInfo,
    compileFileList,
) where

import Data.Aeson
import Data.ByteString as BS
import Data.Text.Lazy as TL
import Data.UUID

import Common.JsonMessage.FeederInfo as CJFI
import Common.JsonMessage.FileList as JMC
import Feeder.Database as D
import Feeder.Database.File as DF
import Feeder.Database.Global as DG

compileFeederInfo :: D.DBCon -> UUID -> IO (ByteString)
compileFeederInfo dbCon pSocketId = do
    (DG.Global gDbUuid rev) <- D.getGlobal dbCon
    let fi = mkFeederInfo pSocketId gDbUuid rev
    return $ BS.toStrict $ encode fi

convertFile :: DF.File -> JMC.File
convertFile (DF.File fId _ rel fSize fTs _) = JMC.File fId (TL.toStrict rel) fSize fTs

compileFileList :: D.DBCon -> IO (ByteString)
compileFileList dbCon = do
    fileSet <- D.getFiles dbCon
    let fl = mkFileList $ convertFile <$> fileSet
    return $ BS.toStrict $ encode fl
