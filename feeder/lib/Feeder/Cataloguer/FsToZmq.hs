{-# LANGUAGE OverloadedStrings #-}

module Feeder.Cataloguer.FsToZmq where

import Control.Concurrent.STM
import Control.Monad
import Feeder.Cataloguer.FsListener as FSL
import System.ZMQ4.Monadic

tqueueToZmq :: TQueue ListenerEvent -> String -> ZMQ z ()
tqueueToZmq queue addrCataloguer = do
    -- Create the socket for forwarding the events
    pub <- socket Pub
    connect pub addrCataloguer

    forever $ do
        _ <- liftIO $ atomically $ readTQueue queue
        send pub [] "fs.change"

start :: [FilePath] -> String -> ZMQ z ()
start ds addrCataloguer = do
    queue <- liftIO $ FSL.start ds
    _ <- async $ tqueueToZmq queue addrCataloguer
    return ()
