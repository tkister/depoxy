{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}

module Feeder.Database (
    DBCon,
    getConnection,
    init,
    getAnchorDirs,
    addAnchorDir,
    getFile,
    getFiles,
    addFile,
    onlyKeepFiles,
    getGlobal,
    Catalogue (..),
    compileCatalogue,
) where

import Control.Concurrent.MVar
import Control.Monad
import Data.Int
import qualified Data.Text.Lazy as TL
import Database.SQLite.Simple
import GHC.Generics
import Prelude hiding (init)

import qualified Feeder.Database.AnchorDir as AD
import qualified Feeder.Database.File as F
import qualified Feeder.Database.Global as G

newtype DBCon = DBCon (MVar Connection)

data Catalogue = Catalogue
    { global :: G.Global
    , files :: [F.File]
    }
    deriving (Generic, Show)

getConnection :: TL.Text -> IO DBCon
getConnection fileName = do
    conn <- open $ TL.unpack fileName
    mvar <- newMVar conn
    return $ DBCon mvar

init :: DBCon -> IO ()
init (DBCon mCon) = do
    conn <- takeMVar mCon

    execute_ conn G.createGlobal
    execute_ conn AD.createAnchorDir
    execute_ conn F.createFile

    G.init conn

    putMVar mCon conn

getAnchorDirs :: DBCon -> IO [AD.AnchorDir]
getAnchorDirs (DBCon mCon) = do
    conn <- takeMVar mCon

    rs <- AD.getAnchorDirs conn

    putMVar mCon conn
    return rs

addAnchorDir :: DBCon -> AD.AnchorDir -> IO AD.AnchorDir
addAnchorDir (DBCon mCon) ad = do
    conn <- takeMVar mCon

    anchorDir <- AD.addAnchorDir conn ad

    putMVar mCon conn
    return anchorDir

getFile :: DBCon -> Int64 -> IO (Maybe F.File)
getFile (DBCon mCon) fileId = do
    conn <- takeMVar mCon

    f <- F.getFile conn fileId

    putMVar mCon conn
    pure f

getFiles :: DBCon -> IO [F.File]
getFiles (DBCon mCon) = do
    conn <- takeMVar mCon

    rs <- F.getFiles conn

    putMVar mCon conn
    return rs

addFile :: DBCon -> F.File -> IO F.File
addFile (DBCon mCon) f = do
    conn <- takeMVar mCon

    nf <- F.addFile conn f

    putMVar mCon conn
    return nf

onlyKeepFiles :: DBCon -> [F.File] -> IO ()
onlyKeepFiles (DBCon mCon) fs = do
    conn <- takeMVar mCon

    F.onlyKeepFiles conn fs

    putMVar mCon conn

getGlobal :: DBCon -> IO G.Global
getGlobal (DBCon mCon) = do
    conn <- takeMVar mCon

    cglobal <- G.getGlobal conn

    putMVar mCon conn
    return cglobal

compileCatalogue :: DBCon -> IO Catalogue
compileCatalogue (DBCon mCon) = do
    conn <- takeMVar mCon

    cglobal <- G.getGlobal conn
    cfiles <- F.getFiles conn

    putMVar mCon conn
    return $ Catalogue cglobal cfiles
