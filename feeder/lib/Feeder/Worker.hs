{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE OverloadedStrings #-}

module Feeder.Worker (addrWorkers, worker) where

import Control.Exception.Safe (bracket, catchIO)
import Control.Monad
import Control.Monad.Trans.Maybe
import Data.Aeson
import qualified Data.ByteString as B
import qualified Data.ByteString.Lazy as BL
import Data.Maybe
import qualified Data.Text.Lazy as TL
import Data.UUID
import GHC.Conc
import System.IO
import System.ZMQ4.Monadic

import qualified Common.JsonMessage.Download as CJD
import qualified Common.JsonMessage.Helpers as CJ
import qualified Common.JsonMessage.SendCatalogue as CJSC
import qualified Feeder.Database as D
import qualified Feeder.Database.File as DF

addrWorkers :: String
addrWorkers = "inproc://workers"

readEvalChunk :: Handle -> CJD.Download -> IO B.ByteString
readEvalChunk handle download = do
    !mChunk <-
        BL.toStrict
            . BL.take (CJD.length download)
            . BL.drop (CJD.start download)
            <$> BL.hGetContents handle
    pure mChunk

getChunk :: D.DBCon -> UUID -> B.ByteString -> MaybeT IO B.ByteString
getChunk dbCon dbUuid msg = do
    -- decode the download request
    download <- MaybeT $ pure $ decode $ BL.fromStrict msg
    liftIO $ print download
    -- check that the database's UUID is correct
    guard $ CJD.dbUuid download == dbUuid

    -- retrieve the database entry
    file <- MaybeT $ D.getFile dbCon (CJD.fileId download)
    let path = TL.unpack $ DF.absolutePath file

    -- retrieve the file chunk
    chunk <- MaybeT $ do
        bracket
            (openBinaryFile path ReadMode)
            hClose
            $ \handle ->
                catchIO
                    (Just <$> readEvalChunk handle download)
                    (\_ -> pure Nothing)

    -- check that the file's chunk is of the correct size
    guard $ fromIntegral (B.length chunk) == CJD.length download

    return chunk

worker :: D.DBCon -> UUID -> String -> ZMQ z ()
worker dbCon dbUuid addrCataloguer = do
    -- Socket for handling file download requests
    rep <- socket Dealer
    connect rep addrWorkers

    -- Socket for forwarding catalogue requests
    pub <- socket Pub
    connect pub addrCataloguer

    _tid <- liftIO $ do
        tid <- myThreadId
        putStrLn $ "Worker " ++ show tid ++ " started"
        pure tid

    forever $ do
        msg <- receive rep
        let mJson = decode $ BL.fromStrict msg :: Maybe Value

        let mSendCatalogue = CJ.mJsonToRec mJson :: Maybe CJSC.SendCatalogue
        let mDownload = CJ.mJsonToRec mJson :: Maybe CJD.Download

        case () of
            _
                | isJust mSendCatalogue -> do
                    send pub [] "worker.cat"
                | isJust mDownload -> do
                    chunk <- liftIO $ runMaybeT $ getChunk dbCon dbUuid msg
                    when (isJust chunk) $ do
                        send rep [SendMore] msg
                        send rep [] (fromJust chunk)
                | otherwise -> pure ()
