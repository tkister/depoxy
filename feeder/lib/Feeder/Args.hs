module Feeder.Args (
    Args (..),
    parseArgs,
) where

import Options.Applicative

data Args = Args
    { version :: Bool
    , config :: String
    }

args :: Parser Args
args =
    Args
        <$> switch
            ( long "version"
                <> help "Display the version number and quit"
            )
        <*> strOption
            ( long "config"
                <> short 'c'
                <> help "Path to configuration file"
                <> showDefault
                <> value "config-feeder.json"
            )

argsInfo :: ParserInfo Args
argsInfo =
    info
        (args <**> helper)
        ( fullDesc
            <> header "depoxy-feeder - A service for feeding files to depoxy-server"
        )

parseArgs :: IO Args
parseArgs = execParser argsInfo
