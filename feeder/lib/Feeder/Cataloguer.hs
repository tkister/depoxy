{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TupleSections #-}

module Feeder.Cataloguer (
    addrCataloguer,
    cataloguerLoop,
) where

import Control.Monad
import qualified Data.Text.Lazy as TL
import Data.UUID (UUID)
import System.ZMQ4.Monadic as ZM

import qualified Data.List.NonEmpty as NE
import Feeder.Cataloguer.CompileMessages
import Feeder.Cataloguer.FsToZmq as FTZ
import Feeder.Cataloguer.RefreshFiles
import qualified Feeder.Config as C
import Feeder.Database as D
import Feeder.Database.AnchorDir as DA

addrCataloguer :: String
addrCataloguer = "inproc://cataloguer"

sendFeederInfo :: D.DBCon -> UUID -> Socket z Pub -> ZMQ z ()
sendFeederInfo dbCon socketId pub = do
    feederInfo <- liftIO $ compileFeederInfo dbCon socketId
    send pub [] feederInfo

sendFileList :: D.DBCon -> UUID -> Socket z Pub -> ZMQ z ()
sendFileList dbCon socketId pub = do
    feederInfo <- liftIO $ compileFeederInfo dbCon socketId
    fileList <- liftIO $ compileFileList dbCon
    sendMulti pub $ NE.fromList [feederInfo, fileList]

cataloguerLoop :: C.Config -> D.DBCon -> UUID -> ZM.ZMQ z ()
cataloguerLoop config dbCon socketId = do
    -- Socket for receiving FS events and catalog requests
    sub <- ZM.socket ZM.Sub
    ZM.subscribe sub ""
    ZM.bind sub addrCataloguer

    -- Socket for publishing the catalog
    let addrSub = C.subscribe $ C.server_addresses config
    pub <- ZM.socket ZM.Pub
    ZM.connect pub addrSub

    -- get the anchor directories from the database
    anchors <- ZM.liftIO $ D.getAnchorDirs dbCon
    let watches = TL.unpack . (DA.path) <$> anchors

    -- Machinery for changing FS notify events to ZMQ inproc messages
    FTZ.start watches addrCataloguer

    -- Refresh the list of existing files
    ZM.liftIO $ do
        putStrLn "Refreshing database…"
        refreshFiles anchors dbCon
        putStrLn "Refreshing database… Done"

    -- Send the first feederInfo message to the server
    sendFeederInfo dbCon socketId pub

    forever $ do
        res <- ZM.poll (60 * 1000) [ZM.Sock sub [ZM.In] Nothing]

        if (null (head res))
            then do
                sendFeederInfo dbCon socketId pub
            else do
                msg <- ZM.receive sub
                case msg of
                    "fs.change" -> do
                        liftIO $ refreshFiles anchors dbCon
                        sendFeederInfo dbCon socketId pub
                    "worker.cat" -> do
                        sendFileList dbCon socketId pub
                    _ -> return () -- Unknown message, ignored
