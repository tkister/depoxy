{-# LANGUAGE OverloadedStrings #-}

module Feeder.Config (
    Database (..),
    Config (..),
    AnchorDir (..),
    ServerAddresses (..),
    Feeder.Config.decode,
) where

import Data.Aeson
import qualified Data.ByteString.Lazy as BL
import qualified Data.Text.Lazy as TL

data Database = Database
    { dbPath :: TL.Text
    }
    deriving (Show)

data ServerAddresses = ServerAddresses
    { subscribe :: String
    , request :: String
    }
    deriving (Show)

type Depth = Int
data AnchorDir = AnchorDir
    { path :: FilePath
    , depth :: Depth
    }
    deriving (Show)

data Config = Config
    { database :: Database
    , anchors :: [AnchorDir]
    , server_addresses :: ServerAddresses
    , workers :: Int
    }
    deriving (Show)

instance FromJSON Database where
    parseJSON = withObject "Database" $ \v ->
        Database
            <$> v .: "path"

instance FromJSON AnchorDir where
    parseJSON = withObject "AnchorDir" $ \v ->
        AnchorDir
            <$> v .: "path"
            <*> v .: "depth"

instance FromJSON ServerAddresses where
    parseJSON = withObject "ServerAddresses" $ \v ->
        ServerAddresses
            <$> v .: "subscribe"
            <*> v .: "request"

instance FromJSON Config where
    parseJSON = withObject "Config" $ \v ->
        Config
            <$> v .: "database"
            <*> v .: "anchors"
            <*> v .: "server-addresses"
            <*> v .: "workers"

decode :: BL.ByteString -> Maybe Config
decode = Data.Aeson.decode
