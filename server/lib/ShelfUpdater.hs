{-# LANGUAGE OverloadedStrings #-}

module ShelfUpdater (
    loop,
) where

import Control.Concurrent.STM
import Control.Monad
import Data.Aeson
import qualified Data.ByteString as BS
import qualified Data.ByteString.Lazy as BL
import Data.List.NonEmpty as NE
import Data.Maybe
import Data.UUID
import System.ZMQ4.Monadic

import Common.JsonMessage.FeederInfo as CJFI
import Common.JsonMessage.FileList as CJFL
import Common.JsonMessage.Helpers as CJ
import Common.JsonMessage.SendCatalogue as CJSC
import Shelf

onFeederInfo :: FeederInfo -> Socket z Router -> TVar Shelf -> ZMQ z ()
onFeederInfo feederInfo router shelf = do
    liftIO $ putStrLn "Feeder info!"

    rawShelf <- liftIO $ readTVarIO shelf
    let needsUpdate = catNeedsUpdate rawShelf feederInfo

    when (needsUpdate) $ do
        liftIO $ putStrLn "Asking for catalogue…"
        let feederId = BL.toStrict $ toByteString $ CJFI.socketId feederInfo
        let payload = BL.toStrict $ encode $ mkSendCatalogue
        let msgs = [feederId, payload]
        sendMulti router $ NE.fromList msgs

upsertTCatalog :: TVar Shelf -> Catalogue -> STM ()
upsertTCatalog shelf newCat = do
    rawShelf <- readTVar shelf
    let newRawShelf = upsertCatalog rawShelf newCat
    writeTVar shelf newRawShelf

onFileList :: FeederInfo -> BS.ByteString -> TVar Shelf -> ZMQ z ()
onFileList fi fl shelf = do
    rawShelf <- liftIO $ readTVarIO shelf
    if catNeedsUpdate rawShelf fi
        then do
            let mJson = decode $ BL.fromStrict fl :: Maybe Value
                mFileList = mJson >>= jsonToRec :: Maybe FileList
                newCat = Catalogue fi <$> mFileList
                mNewShelf = upsertTCatalog shelf <$> newCat

            liftIO $ do
                putStrLn $ "Updating catalog with success: " ++ (show $ isJust newCat)
                maybe (pure ()) atomically mNewShelf
        else do
            liftIO $ putStrLn "Not updating catalog"

loop' :: Socket z Sub -> Socket z Router -> TVar Shelf -> ZMQ z ()
loop' sub router shelf = do
    msg <- receive sub
    let mJson = decode $ BL.fromStrict msg :: Maybe Value

    let mFeederInfo = mJsonToRec mJson :: Maybe FeederInfo

    when (isJust mFeederInfo) $ do
        hasFileList <- moreToReceive sub
        if not hasFileList
            then do
                onFeederInfo (fromJust mFeederInfo) router shelf
                loop' sub router shelf
            else do
                msg' <- receive sub
                onFileList (fromJust mFeederInfo) msg' shelf
                loop' sub router shelf

loop :: String -> String -> TVar Shelf -> IO ()
loop addrSub addrRouter shelf = runZMQ $ do
    sub <- socket Sub
    subscribe sub ""
    bind sub addrSub

    router <- socket Router
    bind router addrRouter

    loop' sub router shelf
