{-# LANGUAGE OverloadedStrings #-}

module App.View.Index (
    index,
) where

import Lucid

index :: Html ()
index = "Nothing to see here."
