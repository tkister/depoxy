{-# LANGUAGE OverloadedStrings #-}

module App.View.List (
    list,
) where

import Data.List
import qualified Data.Text as T
import Lucid as L
import Prelude hiding (id)

import Common.JsonMessage.FeederInfo
import Common.JsonMessage.FileList
import Data.UUID
import Shelf

generateLink :: FeederInfo -> File -> String
generateLink fi file =
    "/get/" ++ (toString $ dbUuid fi) ++ "/" ++ (show $ id file)

renderFile :: FeederInfo -> File -> Html ()
renderFile fi file =
    L.tr_ $ do
        L.td_ $
            L.a_ [L.href_ $ T.pack $ generateLink fi file] $
                L.toHtml $ path file
        L.td_ $ L.toHtml (show $ size file)
        L.td_ $ L.toHtml (show $ ts file)

renderCatalogue :: Catalogue -> Html ()
renderCatalogue cat = do
    let sortedFiles = sortOn path (files $ fileList cat)
    L.table_ $ do
        L.tr_ $
            L.th_ [L.colspan_ "3"] $
                L.toHtml $ "Feeder " ++ (toString . dbUuid . header) cat
        L.tr_ $ do
            L.th_ "Path"
            L.th_ "Size"
            L.th_ "Timestamp"
        mapM_ (renderFile (header cat)) sortedFiles

list :: Shelf -> Html ()
list shelf = do
    mapM_ renderCatalogue shelf
