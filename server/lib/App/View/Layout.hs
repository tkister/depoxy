{-# LANGUAGE OverloadedStrings #-}

module App.View.Layout (
    layout,
) where

import qualified Data.Text.Lazy as TL
import Lucid

layout :: TL.Text -> Html () -> Html ()
layout title content =
    doctypehtml_ $ do
        head_ $ do
            meta_ [charset_ "utf-8"]
            title_ (toHtml title)
            link_ [rel_ "stylesheet", type_ "text/css", href_ "/css/layout.css"]
        body_ $ do
            div_ [class_ "main"] $ do
                h1_ [class_ "logo"] "Depoxy"
                content
