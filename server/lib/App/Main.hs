{-# LANGUAGE OverloadedStrings #-}

module App.Main (
    myApp,
) where

import Control.Concurrent.STM
import Control.Monad.IO.Class (liftIO)
import qualified Lucid as L
import qualified Web.Scotty as S

import App.View.Index
import App.View.Layout
import App.View.List
import Shelf

myApp :: TVar Shelf -> S.ScottyM ()
myApp shelf = do
    S.get "/" $
        S.html $
            L.renderText $
                layout "Depoxy - Index" index

    S.get "/list" $ do
        rawShelf <- liftIO $ readTVarIO shelf
        S.html $
            L.renderText $ layout "Depoxy - List" (list rawShelf)

    S.get "/css/:file" $ do
        file <- S.param "file"
        S.setHeader "Content-Type" "text/css; charset=utf-8"
        S.file $ "server/lib/App/css/" ++ file
