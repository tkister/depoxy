module Lib (
    main,
) where

import Control.Concurrent
import Control.Concurrent.STM
import qualified Web.Scotty as S

import App.Main
import ShelfUpdater

main :: IO ()
main = do
    shelf <- newTVarIO []
    _ <- forkIO $ S.scotty 3000 (myApp shelf)
    loop "tcp://127.0.0.1:3568" "tcp://127.0.0.1:3439" shelf
