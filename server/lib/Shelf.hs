module Shelf (
    Catalogue (..),
    Shelf,
    catNeedsUpdate,
    upsertCatalog,
) where

import Data.Foldable
import Data.Int
import Data.UUID

import Common.JsonMessage.FeederInfo
import Common.JsonMessage.FileList

data Catalogue = Catalogue
    { header :: FeederInfo
    , fileList :: FileList
    }

type Shelf = [Catalogue]

getKnownVersion :: Shelf -> UUID -> Maybe Int64
getKnownVersion cats uuid =
    dbRevision <$> find (\c -> dbUuid c == uuid) (header <$> cats)

catNeedsUpdate :: Shelf -> FeederInfo -> Bool
catNeedsUpdate cats fi =
    let knownVersion = getKnownVersion cats (dbUuid fi)
     in maybe True (\v -> v < (dbRevision fi)) knownVersion

-- does not check that the version increases, responsibility is upon the caller
upsertCatalog :: Shelf -> Catalogue -> Shelf
upsertCatalog cats ncat =
    let nestedHeader = dbUuid . header
        fn c (cs, ex) =
            if nestedHeader c == nestedHeader ncat
                then (ncat : cs, True)
                else (c : cs, ex)
        (ncats, exists) = foldr fn ([], False) cats
     in if exists then ncats else ncat : ncats
