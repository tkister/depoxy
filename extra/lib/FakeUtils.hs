module FakeUtils (
    tsprint,
    uprint,
) where

import qualified Data.ByteString.Char8 as CS
import qualified Data.ByteString.Lazy.Char8 as LCS
import Data.Maybe
import Data.Time
import Data.Time.Format.ISO8601
import qualified Data.UUID as U
import System.ZMQ4.Monadic

tsprint :: String -> ZMQ m ()
tsprint s = liftIO $ do
    now <- getCurrentTime
    let n2 = formatShow iso8601Format now
    putStrLn $ "[" ++ n2 ++ "] " ++ s

uprint :: CS.ByteString -> IO ()
uprint b = print $ show $ fromJust $ U.fromByteString $ LCS.fromStrict b
