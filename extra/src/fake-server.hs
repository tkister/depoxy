{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}

import Control.Monad
import qualified Data.ByteString.Char8 as CS
import qualified Data.List.NonEmpty as NE
import Data.Maybe
import FakeUtils
import System.ZMQ4.Monadic

eventLoop :: Socket z Sub -> Socket z Router -> Timeout -> Int -> Maybe CS.ByteString -> ZMQ z ()
eventLoop sub router timeout count theirId = do
    let socks = [Sock sub [In] Nothing]
    res <- poll (timeout * 1000) socks

    when (res /= [[]]) $ do
        tsprint "Received stuff:"
        msgs <- receiveMulti sub
        liftIO $ do
            uprint $ head msgs
            mapM_ CS.putStrLn $ tail msgs
        eventLoop sub router timeout (count + 1) (Just $ head msgs)

    when (count `mod` 5 == 0 && isJust theirId) $ do
        tsprint "Sending stuff"
        let msgs = [fromJust theirId, "Send me a file!"]
        sendMulti router $ NE.fromList msgs

    eventLoop sub router timeout (count + 1) theirId

main :: IO ()
main = do
    runZMQ $ do
        sub <- socket Sub
        subscribe sub ""
        bind sub "tcp://127.0.0.1:3439"

        router <- socket Router
        bind router "tcp://127.0.0.1:3568"

        liftIO $ putStrLn "Starting event loop"
        eventLoop sub router 2 1 Nothing
