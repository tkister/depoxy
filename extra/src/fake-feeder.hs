{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}

import qualified Data.ByteString.Char8 as CS
import qualified Data.List.NonEmpty as NE
import qualified Data.UUID as U
import Data.UUID.V4 (nextRandom)
import FakeUtils
import System.ZMQ4.Monadic

eventLoop :: Socket z Pub -> Socket z Dealer -> CS.ByteString -> Timeout -> ZMQ z ()
eventLoop pub dealer myId timeout = do
    let socks = [Sock dealer [In] Nothing]
    res <- poll (timeout * 1000) socks

    if res /= [[]]
        then do
            tsprint "Received stuff:"
            msgs <- receiveMulti dealer
            liftIO $ mapM_ CS.putStrLn msgs
        else do
            tsprint "Sending hello"
            sendMulti pub $
                NE.fromList
                    [ myId
                    , "[{\"msgType\":\"hello\",\"dbRevision\":42}"
                    ]

    eventLoop pub dealer myId timeout

main :: IO ()
main = do
    runZMQ $ do
        pub <- socket Pub
        connect pub "tcp://127.0.0.1:3439"

        myId <- liftIO $ do
            x <- nextRandom
            putStrLn $ "My UUID: " ++ U.toString x
            return $ CS.toStrict $ U.toByteString x

        dealer <- socket Dealer
        setIdentity (restrict myId) dealer
        connect dealer "tcp://127.0.0.1:3568"

        liftIO $ putStrLn "Starting event loop"
        eventLoop pub dealer myId 5
