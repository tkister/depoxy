{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}

module Common.JsonMessage.SendCatalogue (
    mkSendCatalogue,
    SendCatalogue (..),
) where

import Data.Aeson
import Data.Aeson.Types
import qualified Data.Text.Lazy as TL
import GHC.Generics

data SendCatalogue = SendCatalogue
    { msgType :: TL.Text
    }
    deriving (Generic, Show)

mkSendCatalogue :: SendCatalogue
mkSendCatalogue = SendCatalogue "sendCatalogue"

instance FromJSON SendCatalogue where
    parseJSON = withObject "SendCatalogue" $ \v -> do
        t <- v .: "msgType" :: Parser String
        case t of
            "sendCatalogue" -> return mkSendCatalogue
            _ -> fail "not the sendCatalogue type"

instance ToJSON SendCatalogue
