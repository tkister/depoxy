module Common.JsonMessage.Helpers where

import Data.Aeson
import Data.Aeson.Key
import Data.Aeson.Types

checkMsgType :: Object -> String -> Parser String
checkMsgType v mt = do
    t <- v .: fromString "msgType" :: Parser String
    if t == mt then pure t else fail "not the expected type"

jsonToRec :: (FromJSON a) => Value -> Maybe a
jsonToRec = parseMaybe parseJSON

mJsonToRec :: (FromJSON a) => Maybe Value -> Maybe a
mJsonToRec = maybe Nothing jsonToRec
