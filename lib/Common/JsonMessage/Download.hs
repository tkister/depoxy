{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}

module Common.JsonMessage.Download (
    Download (..),
    mkDownload,
) where

import Data.Aeson
import Data.Int
import qualified Data.Text.Lazy as TL
import Data.UUID
import GHC.Generics

import Common.JsonMessage.Helpers (checkMsgType)

data Download = Download
    { msgType :: TL.Text
    , dbUuid :: UUID
    , fileId :: Int64
    , start :: Int64
    , length :: Int64
    }
    deriving (Generic, Show)

mkDownload :: UUID -> Int64 -> Int64 -> Int64 -> Download
mkDownload = Download "download"

instance FromJSON Download where
    parseJSON = withObject "Download" $ \v -> do
        _ <- checkMsgType v "download"

        pDbUuid <- v .: "dbUuid"
        pFileId <- v .: "fileId"
        pStart <- v .: "start"
        pLength <- v .: "length"

        pure $ mkDownload pDbUuid pFileId pStart pLength

instance ToJSON Download
