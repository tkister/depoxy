{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}

module Common.JsonMessage.FileList where

import Data.Aeson
import Data.Int
import Data.Text
import Data.Time
import GHC.Generics

import Common.JsonMessage.Helpers

data File = File
    { id :: Int64
    , path :: Text
    , size :: Int64
    , ts :: UTCTime
    }
    deriving (Generic, Show)

instance FromJSON File where
    parseJSON = withObject "File" $ \v -> do
        pId <- v .: "id"
        pPath <- v .: "path"
        pSize <- v .: "size"
        pTs <- v .: "ts"

        pure $ File pId pPath pSize pTs

instance ToJSON File

data FileList = FileList
    { msgType :: Text
    , files :: [File]
    }
    deriving (Generic, Show)

mkFileList :: [File] -> FileList
mkFileList = FileList "fileList"

instance FromJSON FileList where
    parseJSON = withObject "FileList" $ \v -> do
        _ <- checkMsgType v "fileList"

        pFiles <- v .: "files"

        pure $ mkFileList pFiles

instance ToJSON FileList
