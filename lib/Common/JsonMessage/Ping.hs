{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}

module Common.JsonMessage.Ping (
    mkPing,
    Ping (..),
) where

import Data.Aeson
import Data.Aeson.Types
import qualified Data.Text.Lazy as TL
import GHC.Generics

data Ping = Ping
    { msgType :: TL.Text
    }
    deriving (Generic, Show)

mkPing :: Ping
mkPing = Ping "ping"

instance FromJSON Ping where
    parseJSON = withObject "Ping" $ \v -> do
        t <- v .: "msgType" :: Parser String
        case t of
            "ping" -> return mkPing
            _ -> fail "not the ping type"

instance ToJSON Ping
