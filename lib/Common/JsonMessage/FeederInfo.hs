{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}

module Common.JsonMessage.FeederInfo where

import Data.Aeson
import Data.Int
import Data.Text
import Data.UUID
import GHC.Generics

import Common.JsonMessage.Helpers

data FeederInfo = FeederInfo
    { msgType :: Text
    , socketId :: UUID
    , dbUuid :: UUID
    , dbRevision :: Int64
    }
    deriving (Generic, Show)

mkFeederInfo :: UUID -> UUID -> Int64 -> FeederInfo
mkFeederInfo = FeederInfo "feederInfo"

instance FromJSON FeederInfo where
    parseJSON = withObject "Header" $ \v -> do
        _ <- checkMsgType v "feederInfo"

        pSocketId <- v .: "socketId"
        pDbUuid <- v .: "dbUuid"
        pDbRevision <- v .: "dbRevision"

        pure $ mkFeederInfo pSocketId pDbUuid pDbRevision

instance ToJSON FeederInfo
